import React from 'react';
import renderer from 'react-test-renderer';
import { fireEvent, render } from '@testing-library/react-native';

import LoginScreen from '../screens/LoginScreen';
import { handleLogin } from '../api/Endpoints';


const mockNavigation = {
  navigate: jest.fn(),
};

jest.mock('../api/Endpoints', () => ({
  handleLogin: jest.fn().mockResolvedValue(), 
}));

describe('<LoginScreen />', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('renders correctly', () => {
    const tree = renderer.create(<LoginScreen navigation={mockNavigation} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('handles login button press', async () => {
    const { getByPlaceholderText, getByTestId } = render(<LoginScreen navigation={mockNavigation} />);
  
    const emailInput = getByPlaceholderText('Email');
    const passwordInput = getByPlaceholderText('Password');
    fireEvent.changeText(emailInput, 'test@example.com');
    fireEvent.changeText(passwordInput, 'password');
  
    const loginButton = getByTestId('loginButton');
    fireEvent.press(loginButton);
  
    expect(handleLogin).toHaveBeenCalledWith('test@example.com', 'password', mockNavigation);
  });

  it('navigates to Register screen when Register button is pressed', () => {
    const { getByText } = render(<LoginScreen navigation={mockNavigation} />);

    const registerButton = getByText('Register');
    fireEvent.press(registerButton);

    expect(mockNavigation.navigate).toHaveBeenCalledWith('Register');
  });
});
