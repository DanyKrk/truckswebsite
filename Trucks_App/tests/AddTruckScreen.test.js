import React from 'react';
import renderer from 'react-test-renderer';
import { fireEvent, render } from '@testing-library/react-native';

import AddTruckScreen from '../screens/AddTruckScreen';
import { addTruck } from '../api/Endpoints';

const mockNavigation = {
  pop: jest.fn(),
  goBack: jest.fn(),
};

jest.mock('../api/Endpoints', () => ({
  addTruck: jest.fn().mockResolvedValue(),
}));

describe('<AddTruckScreen />', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('renders correctly', () => {
    const tree = renderer.create(<AddTruckScreen navigation={mockNavigation} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('handles add truck button press', async () => {
    const { getByTestId, getByText } = render(<AddTruckScreen navigation={mockNavigation} />);

    const tareInput = getByTestId('tareInput');
    const loadCapacityInput = getByTestId('loadCapacityInput');

    fireEvent.changeText(tareInput, '5000');
    fireEvent.changeText(loadCapacityInput, '10000');

    const addTruckButton = getByTestId('addTruckButton');
    fireEvent.press(addTruckButton);

    expect(addTruck).toHaveBeenCalledWith({
      tare: 5000,
      loadCapacity: 10000,
      autonomyWhenFullyCharged: 0,
      fastChargingTime: 0,
      maximumBatteryCharge: 0,
      isActive: true,
    });
  });

  it('navigates back when "Go back" button is pressed', () => {
    const { getByText } = render(<AddTruckScreen navigation={mockNavigation} />);

    const goBackButton = getByText('Go Back');
    fireEvent.press(goBackButton);

    expect(mockNavigation.goBack).toHaveBeenCalled();
  });
});
