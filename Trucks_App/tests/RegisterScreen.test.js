import React from 'react';
import renderer from 'react-test-renderer';
import { fireEvent, render } from '@testing-library/react-native';

import RegisterScreen from '../screens/RegisterScreen';
import { handleRegister } from '../api/Endpoints';

const mockNavigation = {
  pop: jest.fn(), 
};

jest.mock('../api/Endpoints', () => ({
  handleRegister: jest.fn().mockResolvedValue(), 
}));

describe('<RegisterScreen />', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('renders correctly', () => {
    const tree = renderer.create(<RegisterScreen navigation={mockNavigation} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('handles register button press', async () => {
    const { getByPlaceholderText, getByTestId } = render(<RegisterScreen navigation={mockNavigation} />);
  
    const emailInput = getByPlaceholderText('Email');
    const passwordInput = getByPlaceholderText('Password');
    fireEvent.changeText(emailInput, 'test@example.com');
    fireEvent.changeText(passwordInput, 'password');
  
    const registerButton = getByTestId('registerButton');
    fireEvent.press(registerButton);
  
    expect(handleRegister).toHaveBeenCalledWith('test@example.com', 'password', mockNavigation);
  });

  it('navigates back when "Go back" button is pressed', () => {
    const { getByText } = render(<RegisterScreen navigation={mockNavigation} />);
  
    const goBackButton = getByText('Go back');
    fireEvent.press(goBackButton);
  
    expect(mockNavigation.pop).toHaveBeenCalled();
  });
});
