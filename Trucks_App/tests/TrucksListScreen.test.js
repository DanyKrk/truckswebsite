import React from 'react';
import renderer from 'react-test-renderer';

import App from '../App';

// Mock AsyncStorage
jest.mock('@react-native-async-storage/async-storage', () => ({
  getItem: jest.fn(),
  setItem: jest.fn(),
  removeItem: jest.fn(),
}));

// Mocking the API functions
jest.mock('../api/Endpoints', () => ({
  getAllTrucks: jest.fn(),
  deleteTruck: jest.fn(),
}));

describe('TruckListScreen', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });


describe('<App />', () => {
  it('has 1 child', () => {
    const tree = renderer.create(<App />).toJSON();
    expect(tree.children.length).toBe(1);
  });
});

});