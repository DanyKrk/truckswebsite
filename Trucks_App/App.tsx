import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import LoginScreen from './screens/LoginScreen';
import TruckListScreen from './screens/TrucksListScreen';
import RegisterScreen from './screens/RegisterScreen';
import AddTruckScreen from './screens/AddTruckScreen';
import TruckFilterScreen from './screens/TruckFilterScreen';
import { ScrollView, Text, View } from 'react-native';

const Stack = createStackNavigator();

function App() {
  return (
    // <ScrollView contentContainerStyle={{flex:1}}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="LoginScreen" screenOptions={{ headerShown: false }}>
          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="Register" component={RegisterScreen} />
          <Stack.Screen name="TruckList" component={TruckListScreen} />
          <Stack.Screen name="AddTruck" component={AddTruckScreen} />
          <Stack.Screen name="TruckFilter" component={TruckFilterScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    // </ScrollView>
  );
}

export default App;
