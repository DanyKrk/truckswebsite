import AsyncStorage from "@react-native-async-storage/async-storage";

const BASE_URL = process.env.EXPO_PUBLIC_BACKEND_URL;

export const getAllTrucks = async () => {
    try {
        const storedAccessToken = await AsyncStorage.getItem('accessToken');

        if (!storedAccessToken) {
          throw new Error('Access token not found');
        }
  
        const response = await fetch(BASE_URL + 'api/Truck', {
          headers: {
            'Authorization': `Bearer ${storedAccessToken}`
          }
        });
        if (!response.ok) {
            throw new Error('Failed to fetch data');
        }
        const data = await response.json();
        return data;
    } catch (error) {
        throw error;
    }
};


export const getAllTrucksFiltered = async (filters) => {
  try {
    console.log("dupa")
    const storedAccessToken = await AsyncStorage.getItem('accessToken');

    if (!storedAccessToken) {
      throw new Error('Access token not found');
    }

    const response = await fetch(`${BASE_URL}api/Truck/filter?${new URLSearchParams(filters)}`, {
      headers: {
        'Authorization': `Bearer ${storedAccessToken}`
      }
    });
    if (!response.ok) {
      throw new Error('Failed to fetch data');
    }
    const data = await response.json();
    return data;
  } catch (error) {
    throw error;
  }
};


export const addTruck = async (truckData) => {
    try {
        const storedAccessToken = await AsyncStorage.getItem('accessToken');

        if (!storedAccessToken) {
          throw new Error('Access token not found');
        }

        const response = await fetch(BASE_URL + 'api/Truck', {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${storedAccessToken}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(truckData)
        });

        if (!response.ok) {
            throw new Error('Failed to add truck');
        }

    } catch (error) {
        throw error;
    }
};

export const deleteTruck = async (id) => {
  try {
      const storedAccessToken = await AsyncStorage.getItem('accessToken');

      if (!storedAccessToken) {
        throw new Error('Access token not found');
      }

      const response = await fetch(`${BASE_URL}api/Truck/${id}`, {
          method: 'DELETE',
          headers: {
              'Authorization': `Bearer ${storedAccessToken}`,
          }
      });

      if (!response.ok) {
          throw new Error('Failed to delete truck');
      }
  } catch (error) {
      throw error;
  }
};

export const handleRegister = async (email, password, navigation) => {
    try {
        const response = await fetch(BASE_URL + 'register', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email, password }),
            credentials: 'include'
        });

        if (!response.ok) {
            throw new Error('Failed to register');
        }
        navigation.navigate('Login');
    } catch (error) {
        console.error('Registration failed', error);
        throw new Error('Registration failed. Please try again.');
    }
};

export const handleLogin = async (email, password, navigation) => {
  try {
    const response = await fetch(BASE_URL + 'login', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password,
        twoFactorCode: '',
        twoFactorRecoveryCode: ''
      }),
      credentials: 'include'
    });

    if (!response.ok) {
      throw new Error('Failed to log in');
    }

    const loginData = await response.json();

    // Store access token securely using AsyncStorage
    await AsyncStorage.setItem('accessToken', loginData.accessToken);

    navigation.navigate('TruckFilter');
  } catch (error) {
    console.error('Login failed', error);
    throw new Error('Login failed. Please try again.');
  }
};