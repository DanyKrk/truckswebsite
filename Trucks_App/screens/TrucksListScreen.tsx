import React, { useState, useEffect, useCallback } from 'react';
import { View, Text, FlatList, ActivityIndicator, Button, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import { getAllTrucks, deleteTruck } from '../api/Endpoints'; 
import { useFocusEffect } from '@react-navigation/native';

const TruckListScreen = ({navigation}) => {
  const [trucks, setTrucks] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetchTrucks();
  }, []);

  useFocusEffect(
    useCallback(() => {
      fetchTrucks();
    }, [])
  );

  const fetchTrucks = async () => {
    try {
      const response = await getAllTrucks();
      const data = response.data; 
      setTrucks(data);
      setLoading(false);
    } catch (error) {
      console.error('Error fetching trucks:', error);
      setLoading(false);
    }
  };

  const handleDeleteTruck = async (id) => {
    try {
      await deleteTruck(id);
      fetchTrucks();
    } catch (error) {
      console.error('Error deleting truck:', error);
    }
  };

  if (loading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color="blue" />
      </View>
    );
  }

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Button title="Add truck" onPress={() => navigation.navigate("AddTruck")} />
      <Button title="Filter and paginate" onPress={() => navigation.navigate("TruckFilter")} />
      <Button title="Logout" onPress={() => navigation.goBack()} />
      {trucks.length === 0 ? (
        <Text>No trucks available</Text>
      ) : (
        <FlatList
          data={trucks}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item }) => (
            <View style={styles.truckContainer}>
              <Text>ID: {item.id}</Text>
              <Text>Tare: {item.tare}</Text>
              <Text>Load Capacity: {item.loadCapacity}</Text>
              <Text>Maximum Battery Charge: {item.maximumBatteryCharge}</Text>
              <Text>Autonomy When Fully Charged: {item.autonomyWhenFullyCharged}</Text>
              <Text>Fast Charging Time: {item.fastChargingTime}</Text>
              <Text>Active: {item.isActive ? 'Yes' : 'No'}</Text>
              <TouchableOpacity onPress={() => handleDeleteTruck(item.id)}>
                <View style={styles.deleteButton}>
                  <Text style={styles.deleteButtonText}>Delete</Text>
                </View>
              </TouchableOpacity>
            </View>
          )}
        />
      )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  truckContainer: {
    marginBottom: 10,
  },
  deleteButton: {
    backgroundColor: 'red',
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginTop: 5,
    borderRadius: 5,
  },
  deleteButtonText: {
    color: 'white',
  },
});

export default TruckListScreen;
