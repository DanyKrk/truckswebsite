import React, { useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet, Switch } from 'react-native';
import { addTruck } from '../api/Endpoints';

const AddTruckScreen = ({navigation}) => {
  const [tare, setTare] = useState('0');
  const [loadCapacity, setLoadCapacity] = useState('0');
  const [maximumBatteryCharge, setMaximumBatteryCharge] = useState('0');
  const [autonomyWhenFullyCharged, setAutonomyWhenFullyCharged] = useState('0');
  const [fastChargingTime, setFastChargingTime] = useState('0');
  const [isActive, setIsActive] = useState(true);

  const handleAddTruck = async () => {
    try {
      const truckData = {
        tare: parseFloat(tare),
        loadCapacity: parseFloat(loadCapacity),
        maximumBatteryCharge: parseFloat(maximumBatteryCharge),
        autonomyWhenFullyCharged: parseFloat(autonomyWhenFullyCharged),
        fastChargingTime: parseFloat(fastChargingTime),
        isActive
      };
      await addTruck(truckData);
      navigation.pop()
    } catch (error) {
      console.error('Failed to add truck', error);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.heading}>Add Truck</Text>
      <View style={styles.inputContainer}>
        <Text style={styles.label}>Tare:</Text>
        <TextInput
          style={styles.input}
          placeholder="0"
          value={tare}
          onChangeText={setTare}
          keyboardType="numeric"
          testID="tareInput"
        />
      </View>
      <View style={styles.inputContainer}>
        <Text style={styles.label}>Load Capacity:</Text>
        <TextInput
          style={styles.input}
          placeholder="0"
          value={loadCapacity}
          onChangeText={setLoadCapacity}
          keyboardType="numeric"
          testID="loadCapacityInput"
        />
      </View>
      <View style={styles.inputContainer}>
        <Text style={styles.label}>Maximum Battery Charge:</Text>
        <TextInput
          style={styles.input}
          placeholder="0"
          value={maximumBatteryCharge}
          onChangeText={setMaximumBatteryCharge}
          keyboardType="numeric"
        />
      </View>
      <View style={styles.inputContainer}>
        <Text style={styles.label}>Autonomy When Fully Charged:</Text>
        <TextInput
          style={styles.input}
          placeholder="0"
          value={autonomyWhenFullyCharged}
          onChangeText={setAutonomyWhenFullyCharged}
          keyboardType="numeric"
        />
      </View>
      <View style={styles.inputContainer}>
        <Text style={styles.label}>Fast Charging Time:</Text>
        <TextInput
          style={styles.input}
          placeholder="0"
          value={fastChargingTime}
          onChangeText={setFastChargingTime}
          keyboardType="numeric"
        />
      </View>
      <View style={styles.inputContainer}>
        <Text style={styles.label}>Is Active:</Text>
        <Switch
          value={isActive}
          onValueChange={setIsActive}
        />
      </View>
      <Button title="Add Truck" testID="addTruckButton" onPress={handleAddTruck} />
      <Button title="Go Back" onPress={() => navigation.goBack()} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  heading: {
    fontSize: 24,
    marginBottom: 20,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  label: {
    marginRight: 10,
  },
  input: {
    flex: 1,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 10,
  },
});

export default AddTruckScreen;
