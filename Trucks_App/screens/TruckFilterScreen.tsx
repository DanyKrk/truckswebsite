import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, Button, FlatList, ScrollView, TouchableOpacity, StyleSheet, Switch, Pressable } from 'react-native';
import { deleteTruck, getAllTrucksFiltered } from '../api/Endpoints';

const TruckFilterScreen = ({navigation}) => {
  const [filters, setFilters] = useState({
    TareLowerBound: -1,
    TareUpperBound: 100,
    LoadCapacityLowerBound: -1,
    LoadCapacityUpperBound: 100,
    MaximumBatteryChargeLowerBound: -1,
    MaximumBatteryChargeUpperBound: 100,
    AutonomyWhenFullyChargedLowerBound: -1,
    AutonomyWhenFullyChargedUpperBound: 100,
    FastChargingTimeLowerBound: -1,
    FastChargingTimeUpperBound: 100,
    IsActive: true,
    PageNumber: 1,
    PageSize: 5,
  });
  const [trucks, setTrucks] = useState([]);
  const [totalTrucks, setTotalTrucks] = useState(0);
  const [drawerVisible, setDrawerVisible] = useState(false);

  const fetchTrucks = async () => {
    try {
      const data = await getAllTrucksFiltered(filters); 
      setTrucks(data.data);
      setTotalTrucks(data.data.length);
    } catch (error) {
      console.error('Error fetching trucks:', error.message);
    }
  };

  useEffect(() => {
    fetchTrucks();
  }, [filters]);

  const handleDeleteTruck = async (id) => {
    try {
      await deleteTruck(id);
      fetchTrucks();
    } catch (error) {
      console.error('Error deleting truck:', error);
    }
  };

  const handleFilterChange = (field, value) => {
    setFilters(prevFilters => ({
      ...prevFilters,
      [field]: value,
    }));
  };

  const handleFilterApply = () => {
    setFilters(prevFilters => ({
      ...prevFilters,
      PageNumber: 1,
    }));
    setDrawerVisible(false); // Close the drawer after applying filters
  };

  const toggleDrawer = () => {
    setDrawerVisible(!drawerVisible);
  };

  const handlePagination = () => {
    setFilters(prevFilters => ({
      ...prevFilters,
      PageNumber: prevFilters.PageNumber + 1,
    }));
  };

  return (
    <View>
       <TouchableOpacity onPress={toggleDrawer}>
            <Text style={{ textAlign: 'center', padding: 10 }}>Open Filters</Text>
        </TouchableOpacity>
        {drawerVisible && (
            <View style={{ backgroundColor: 'lightgrey', padding: 10 }}>
            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Filters</Text>
            <View style={styles.inputContainer}>
            <Text style={styles.label}>Tare Lower Bound:</Text>
            <TextInput
              placeholder="Enter Tare Lower Bound"
              value={filters.TareLowerBound.toString()}
              onChangeText={value => handleFilterChange('TareLowerBound', value)}
              keyboardType="numeric"
              style={styles.input}
            />
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.label}>Tare Upper Bound:</Text>
            <TextInput
              placeholder="Enter Tare Upper Bound"
              value={filters.TareUpperBound.toString()}
              onChangeText={value => handleFilterChange('TareUpperBound', value)}
              keyboardType="numeric"
              style={styles.input}
            />
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.label}>Load Capacity Lower Bound:</Text>
            <TextInput
              placeholder="Enter Load Capacity Lower Bound"
              value={filters.LoadCapacityLowerBound.toString()}
              onChangeText={value => handleFilterChange('LoadCapacityLowerBound', value)}
              keyboardType="numeric"
              style={styles.input}
            />
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.label}>Load Capacity Upper Bound:</Text>
            <TextInput
              placeholder="Enter Load Capacity Upper Bound"
              value={filters.LoadCapacityUpperBound.toString()}
              onChangeText={value => handleFilterChange('LoadCapacityUpperBound', value)}
              keyboardType="numeric"
              style={styles.input}
            />
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.label}>Maximum Battery Charge Lower Bound:</Text>
            <TextInput
              placeholder="Enter Maximum Battery Charge Lower Bound"
              value={filters.MaximumBatteryChargeLowerBound.toString()}
              onChangeText={value => handleFilterChange('MaximumBatteryChargeLowerBound', value)}
              keyboardType="numeric"
              style={styles.input}
            />
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.label}>Maximum Battery Charge Upper Bound:</Text>
            <TextInput
              placeholder="Enter Maximum Battery Charge Upper Bound"
              value={filters.MaximumBatteryChargeUpperBound.toString()}
              onChangeText={value => handleFilterChange('MaximumBatteryChargeUpperBound', value)}
              keyboardType="numeric"
              style={styles.input}
            />
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.label}>Autonomy When Fully Charged Lower Bound:</Text>
            <TextInput
              placeholder="Enter Autonomy When Fully Charged Lower Bound"
              value={filters.AutonomyWhenFullyChargedLowerBound.toString()}
              onChangeText={value => handleFilterChange('AutonomyWhenFullyChargedLowerBound', value)}
              keyboardType="numeric"
              style={styles.input}
            />
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.label}>Autonomy When Fully Charged Upper Bound:</Text>
            <TextInput
              placeholder="Enter Autonomy When Fully Charged Upper Bound"
              value={filters.AutonomyWhenFullyChargedUpperBound.toString()}
              onChangeText={value => handleFilterChange('AutonomyWhenFullyChargedUpperBound', value)}
              keyboardType="numeric"
              style={styles.input}
            />
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.label}>Fast Charging Time Lower Bound:</Text>
            <TextInput
              placeholder="Enter Fast Charging Time Lower Bound"
              value={filters.FastChargingTimeLowerBound.toString()}
              onChangeText={value => handleFilterChange('FastChargingTimeLowerBound', value)}
              keyboardType="numeric"
              style={styles.input}
            />
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.label}>Fast Charging Time Upper Bound:</Text>
            <TextInput
              placeholder="Enter Fast Charging Time Upper Bound"
              value={filters.FastChargingTimeUpperBound.toString()}
              onChangeText={value => handleFilterChange('FastChargingTimeUpperBound', value)}
              keyboardType="numeric"
              style={styles.input}
            />
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.label}>IsActive:</Text>
            <Switch
              value={filters.IsActive}
              onValueChange={value => handleFilterChange('IsActive', value)}
              style={styles.switch}
            />
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.label}>Page Number:</Text>
            <TextInput
              placeholder="Enter Page Number"
              value={filters.PageNumber.toString()}
              onChangeText={value => handleFilterChange('PageNumber', parseInt(value))}
              keyboardType="numeric"
              style={styles.input}
            />
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.label}>Page Size:</Text>
            <TextInput
              placeholder="Enter Page Size"
              value={filters.PageSize.toString()}
              onChangeText={value => handleFilterChange('PageSize', parseInt(value))}
              keyboardType="numeric"
              style={styles.input}
            />
          </View>

            <Button title="Apply Filters" onPress={handleFilterApply} />
            </View>
        )}
      <View style={styles.container}>
        <Button title="Add truck" onPress={() => navigation.navigate("AddTruck")} />
        <Button title="Go back" onPress={() => navigation.goBack()} />
        {trucks.length === 0 ? (
            <Text>No trucks available</Text>
        ) : (
          trucks.map((item) => (
            <View key={item.id} style={styles.truckContainer}>
              <Text>ID: {item.id}</Text>
              <Text>Tare: {item.tare}</Text>
              <Text>Load Capacity: {item.loadCapacity}</Text>
              <Text>Maximum Battery Charge: {item.maximumBatteryCharge}</Text>
              <Text>Autonomy When Fully Charged: {item.autonomyWhenFullyCharged}</Text>
              <Text>Fast Charging Time: {item.fastChargingTime}</Text>
              <Text>Active: {item.isActive ? 'Yes' : 'No'}</Text>
              <TouchableOpacity onPress={() => handleDeleteTruck(item.id)}>
                <View style={styles.deleteButton}>
                  <Text style={styles.deleteButtonText}>Delete</Text>
                </View>
              </TouchableOpacity>
            </View>
          ))          
        )}
        </View>
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
      flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'center',
      paddingVertical: 20,
    },
    loadingContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    truckContainer: {
      marginBottom: 10,
    },
    deleteButton: {
      backgroundColor: 'red',
      paddingHorizontal: 10,
      paddingVertical: 5,
      marginTop: 5,
      borderRadius: 5,
    },
    deleteButtonText: {
      color: 'white',
    },
    inputContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      marginVertical: 5,
    },
    label: {
      marginRight: 10,
      fontSize: 16,
    },
    input: {
      flex: 1,
      borderWidth: 1,
      borderColor: 'grey',
      padding: 5,
    },
    switch: {
      marginLeft: 10,
    },
  });

export default TruckFilterScreen;
