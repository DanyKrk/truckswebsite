# TrucksWebsite

Project created by [Daniel Krzykawski](https://gitlab.com/DanyKrk) for ZTP classes at Cracow University of Technlogy.

### Getting started

##### With docker

- go to `Docker` folder and run `docker compose up` there. The app should be available at localhost:3000.

### Tests

##### Backend

Backend tests are created using the xUnit. xUnit is a popular testing framework for .NET applications. To fire them, go into the root folder and run `dotnet test` command. 

##### Fronted

Frontend tests are created using the jest framework. To fire them, go into the `Trucks_App` folder and run `npm run test` command. 

### About the app

The app was created to allow storage and displaying data of a company's fleet of trucks.

Users have the ability to:

- create an account
- log in
- browse the list of trucks and filter it
- add a truck
- delete a truck

<img src="images/usecases_diagram.png" alt="swagger" width="800"/>  

### Techonologies used

#### Backend
Framework: .NET Core

Main External Technologies Used:

- Entity Framework Core: ORM (Object-Relational Mapping) library for .NET Core. Facilitates database interactions by mapping .NET objects to database tables.
- AutoMapper: A library for object-to-object mapping in .NET. Simplifies the mapping between DTOs (Data Transfer Objects) and entity models, reducing boilerplate code.
- Swagger: Integrated for API documentation and testing. Provides a convenient way to describe, produce, and consume APIs with interactive documentation.
- ASP.NET Core Identity: Provides APIs for user authentication, authorization, and management. Essential for building secure web applications with user-specific features.
- Cors: Cross-Origin Resource Sharing middleware for ASP.NET Core. Configured to allow cross-origin requests from specified frontend URLs, enhancing interoperability.
- Microsoft.AspNetCore.Mvc.Core: Core components of ASP.NET Core MVC (Model-View-Controller) framework. Includes services and middleware for building web APIs.
- Microsoft.EntityFrameworkCore.SqlServer: EF Core provider for SQL Server. Used to configure EF Core to work with SQL Server databases, simplifying database operations.
- Microsoft.EntityFrameworkCore.InMemory: EF Core in-memory database provider. Used for testing purposes to simulate a database in memory, ensuring reliable tests.
###### Swagger
<img src="images/swagger.png" alt="swagger" width="800"/>  

#### Frontend
Framework/Library: React Native

Main External Technologies Used:

- @react-navigation/native and @react-navigation/stack: Libraries for routing and navigation in React Native applications.
- @expo/metro-runtime: A runtime package for the Expo development environment, providing tools and utilities for building and running React Native apps with Expo.
- @react-native-async-storage/async-storage: A library for asynchronous storage in React Native applications, used for storing data locally on the device.
- expo: A platform and set of tools for building and deploying universal, native apps for Android, iOS, and the web with JavaScript and React.
- react: The core library for building user interfaces in React applications.

#### Database

The app uses the SqlServer database.

Database schema:

<img src="images/schema.png" alt="database-schema" width="500"/>

### Pipeline

<img src="images/pipeline.png" alt="pipeline" width="800"/>

### App screens

##### Sign up

<img src="images/register.png" alt="register" width="800"/>

##### Sign in

<img src="images/login.png" alt="login" width="800"/>

##### Truck list

User can browse the list and delete choosen trucks.
<img src="images/trucklist.png" alt="trucklist" width="800"/>

User can use an extendable bar to filter the displayed trucks and specify the page size together with the page number.
<img src="images/filters.png" alt="dish-list-filtered" width="800"/>

##### Add truck screen

User can add a new truck to the list using a screen with a form
<img src="images/addtruck.png" alt="add-truck" width="800"/>

