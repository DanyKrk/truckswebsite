using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Trucks_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TruckController : ControllerBase
    {
        private readonly ITruckService _truckService;

        public TruckController(ITruckService truckService)
        {
            this._truckService = truckService;
        }

        [HttpGet, Authorize]
        [ProducesResponseType(typeof(ServiceResponse<GetTruckDto>), 200)]
        [ProducesResponseType(typeof(ServiceResponse<GetTruckDto>), 404)]
        public async Task<ActionResult<ServiceResponse<List<GetTruckDto>>>> Get()
        {
            var response = await _truckService.GetAllTrucks();
            if (response.Data == null)
                return NotFound(response);

            return Ok(response);
        }

        [HttpGet("{id}"), Authorize]
        [ProducesResponseType(typeof(ServiceResponse<GetTruckDto>), 200)]
        [ProducesResponseType(typeof(ServiceResponse<GetTruckDto>), 404)]
        public async Task<ActionResult<ServiceResponse<GetTruckDto>>> GetSingle(int id)
        {
            var response = await _truckService.GetTruckById(id);
            if (response.Data == null)
                return NotFound(response);

            return Ok(response);
        }

        [HttpGet("filter"), Authorize]
        [ProducesResponseType(typeof(ServiceResponse<GetTruckDto>), 200)]
        [ProducesResponseType(typeof(ServiceResponse<GetTruckDto>), 404)]
        public async Task<ActionResult<ServiceResponse<List<GetTruckDto>>>> FilterTrucks([FromQuery] TruckFilterDto filters)
        {
            var response = await _truckService.FilterAndPaginateTrucks(filters);
            if (response.Data == null)
                return NotFound(response);
            return Ok(response);
        }

        [HttpPost, Authorize]
        public async Task<ActionResult<ServiceResponse<List<GetTruckDto>>>> AddTruck(AddTruckDto newTruck)
        {
            return Ok(await _truckService.AddTruck(newTruck));
        }

        [HttpPut, Authorize]
        [ProducesResponseType(typeof(ServiceResponse<GetTruckDto>), 200)]
        [ProducesResponseType(typeof(ServiceResponse<GetTruckDto>), 404)]
        public async Task<ActionResult<ServiceResponse<List<GetTruckDto>>>> UpdateTruck(UpdateTruckDto updatedTruck)
        {
            var response = await _truckService.UpdateTruck(updatedTruck);
            if (response.Data is null)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpDelete("{id}"), Authorize]
        [ProducesResponseType(typeof(ServiceResponse<GetTruckDto>), 200)]
        [ProducesResponseType(typeof(ServiceResponse<GetTruckDto>), 404)]
        public async Task<ActionResult<ServiceResponse<List<GetTruckDto>>>> DeleteTruck(int id)
        {
            var response = await _truckService.DeleteTruck(id);
            if (response.Data is null)
            {
                return NotFound(response);
            }
            return Ok(response);
        }
    }
}