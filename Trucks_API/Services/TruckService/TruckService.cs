using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Trucks_API.Services.TruckService
{
    public class TruckService : ITruckService
    {
        private readonly IMapper _mapper;
        private readonly DataContext _context;

        public TruckService(IMapper mapper, DataContext context)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<ServiceResponse<List<GetTruckDto>>> GetAllTrucks()
        {
            var serviceResponse = new ServiceResponse<List<GetTruckDto>>();

            try
            {
                var dbTrucks = await _context.Trucks.ToListAsync();
                serviceResponse.Data = dbTrucks.Select(t => _mapper.Map<GetTruckDto>(t)).ToList();
                serviceResponse.Success = true;
                serviceResponse.Message = "Trucks retrieved successfully.";
            }
            catch (Exception ex)
            {
                serviceResponse.Success = false;
                serviceResponse.Message = $"An error occurred while retrieving trucks: {ex.Message}";
            }

            return serviceResponse;
        }
        public async Task<ServiceResponse<List<GetTruckDto>>> FilterAndPaginateTrucks(TruckFilterDto filters)
        {
            var response = new ServiceResponse<List<GetTruckDto>>();

            try
            {
                var query = _context.Trucks.AsQueryable();
                if (filters.TareLowerBound.HasValue)
                    query = query.Where(t => t.Tare >= filters.TareLowerBound);
                if (filters.TareUpperBound.HasValue)
                    query = query.Where(t => t.Tare <= filters.TareUpperBound);

                if (filters.LoadCapacityLowerBound.HasValue)
                    query = query.Where(t => t.LoadCapacity >= filters.LoadCapacityLowerBound);
                if (filters.LoadCapacityUpperBound.HasValue)
                    query = query.Where(t => t.LoadCapacity <= filters.LoadCapacityUpperBound);

                if (filters.MaximumBatteryChargeLowerBound.HasValue)
                    query = query.Where(t => t.MaximumBatteryCharge >= filters.MaximumBatteryChargeLowerBound);
                if (filters.MaximumBatteryChargeUpperBound.HasValue)
                    query = query.Where(t => t.MaximumBatteryCharge <= filters.MaximumBatteryChargeUpperBound);

                if (filters.AutonomyWhenFullyChargedLowerBound.HasValue)
                    query = query.Where(t => t.AutonomyWhenFullyCharged >= filters.AutonomyWhenFullyChargedLowerBound);
                if (filters.AutonomyWhenFullyChargedUpperBound.HasValue)
                    query = query.Where(t => t.AutonomyWhenFullyCharged <= filters.AutonomyWhenFullyChargedUpperBound);

                if (filters.FastChargingTimeLowerBound.HasValue)
                    query = query.Where(t => t.FastChargingTime >= filters.FastChargingTimeLowerBound);
                if (filters.FastChargingTimeUpperBound.HasValue)
                    query = query.Where(t => t.FastChargingTime <= filters.FastChargingTimeUpperBound);


                if (filters.IsActive.HasValue)
                    query = query.Where(t => t.IsActive == filters.IsActive);

                var totalCount = await query.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / filters.PageSize);

                var paginatedTrucks = await query
                    .Skip((filters.PageNumber - 1) * filters.PageSize)
                    .Take(filters.PageSize)
                    .ToListAsync();

                response.Data = _mapper.Map<List<GetTruckDto>>(paginatedTrucks);
                response.Success = true;
                response.Message = $"Retrieved {paginatedTrucks.Count} trucks out of {totalCount} total.";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"An error occurred while filtering and paginating trucks: {ex.Message}";
            }

            return response;
        }

        public async Task<ServiceResponse<GetTruckDto>> GetTruckById(int id)
        {
            var serviceResponse = new ServiceResponse<GetTruckDto>();

            try
            {
                var dbTruck = await _context.Trucks.FirstOrDefaultAsync(t => t.Id == id);
                if (dbTruck == null)
                {
                    serviceResponse.Success = false;
                    serviceResponse.Message = $"Truck with Id '{id}' not found";
                    return serviceResponse;
                }

                serviceResponse.Data = _mapper.Map<GetTruckDto>(dbTruck);
                serviceResponse.Success = true;
                serviceResponse.Message = "Truck retrieved successfully.";
            }
            catch (Exception ex)
            {
                serviceResponse.Success = false;
                serviceResponse.Message = $"Failed to retrieve truck: {ex.Message}";
            }

            return serviceResponse;
        }
        public async Task<ServiceResponse<GetTruckDto>> AddTruck(AddTruckDto newTruck)
        {
            var serviceResponse = new ServiceResponse<GetTruckDto>();

            try
            {
                var truck = _mapper.Map<Truck>(newTruck);
                _context.Trucks.Add(truck);
                await _context.SaveChangesAsync();

                var addedTruck = await _context.Trucks.FindAsync(truck.Id);
                if (addedTruck == null)
                {
                    throw new Exception("Failed to retrieve the newly added truck.");
                }

                serviceResponse.Data = _mapper.Map<GetTruckDto>(addedTruck);
                serviceResponse.Success = true;
                serviceResponse.Message = "Truck added successfully.";
            }
            catch (Exception ex)
            {
                serviceResponse.Success = false;
                serviceResponse.Message = $"An error occurred while adding the truck: {ex.Message}";
            }

            return serviceResponse;
        }

        public async Task<ServiceResponse<GetTruckDto>> UpdateTruck(UpdateTruckDto updatedTruck)
        {
            var serviceResponse = new ServiceResponse<GetTruckDto>();

            try
            {
                var dbTruck = await _context.Trucks.FirstOrDefaultAsync(t => t.Id == updatedTruck.Id);
                if (dbTruck is null)
                {
                    throw new Exception($"Truck with Id '{updatedTruck.Id}' not found");
                }

                _mapper.Map(updatedTruck, dbTruck);
                await _context.SaveChangesAsync();
                serviceResponse.Data = _mapper.Map<GetTruckDto>(dbTruck);
                serviceResponse.Success = true;
                serviceResponse.Message = "Truck updated successfully.";
            }
            catch (Exception ex)
            {
                serviceResponse.Success = false;
                serviceResponse.Message = $"An error occurred while updating the truck: {ex.Message}";
            }
            return serviceResponse;
        }

        public async Task<ServiceResponse<GetTruckDto>> DeleteTruck(int id)
        {
            var serviceResponse = new ServiceResponse<GetTruckDto>();

            try
            {
                var dbTruck = await _context.Trucks.FirstOrDefaultAsync(t => t.Id == id);
                if (dbTruck is null)
                {
                    throw new Exception($"Truck with Id '{id}' not found");
                }

                _context.Trucks.Remove(dbTruck);
                await _context.SaveChangesAsync();

                serviceResponse.Data = _mapper.Map<GetTruckDto>(dbTruck);
                serviceResponse.Success = true;
                serviceResponse.Message = "Truck deleted successfully.";
            }
            catch (Exception ex)
            {
                serviceResponse.Success = false;
                serviceResponse.Message = $"An error occurred while deleting the truck: {ex.Message}";
            }

            return serviceResponse;
        }

    }
}