public class TruckFilterDto
{
    public int? TareLowerBound { get; set; }
    public int? TareUpperBound { get; set; }
    public int? LoadCapacityLowerBound { get; set; }
    public int? LoadCapacityUpperBound { get; set; }
    public int? MaximumBatteryChargeLowerBound { get; set; }
    public int? MaximumBatteryChargeUpperBound { get; set; }
    public int? AutonomyWhenFullyChargedLowerBound { get; set; }
    public int? AutonomyWhenFullyChargedUpperBound { get; set; }
    public int? FastChargingTimeLowerBound { get; set; }
    public int? FastChargingTimeUpperBound { get; set; }
    public bool? IsActive { get; set; }
    public int PageNumber { get; set; } = 1;
    public int PageSize { get; set; } = 10;
}