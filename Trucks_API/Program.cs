global using Trucks_API.Models;
global using Trucks_API.Services;
global using Trucks_API.Services.TruckService;
global using Trucks_API.Dtos.Truck;
global using AutoMapper;
global using Microsoft.EntityFrameworkCore;
global using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
global using Trucks_API.Data;
global using Microsoft.AspNetCore.Identity;
global using Microsoft.OpenApi.Models;
global using Swashbuckle.AspNetCore.Filters;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDbContext<DataContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"),
    sqlServerOptionsAction: sqlOptions =>
        {
            sqlOptions.EnableRetryOnFailure();
        }
));
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey
    });

    options.OperationFilter<SecurityRequirementsOperationFilter>();
});
builder.Services.AddAutoMapper(typeof(Program).Assembly);
builder.Services.AddScoped<ITruckService, TruckService>();
string FRONTEND_DOCKER_URL = Environment.GetEnvironmentVariable("FRONTEND_DOCKER_URL");
string FRONTEND_LOCAL_URL = Environment.GetEnvironmentVariable("FRONTEND_LOCAL_URL");
builder.Services.AddCors(options =>
    {
        // options.AddDefaultPolicy(
        //     policy =>
        //     {
        //         policy.AllowAnyOrigin()
        //             .AllowAnyHeader()
        //             .AllowAnyMethod();
        //     });
        options.AddPolicy("AllowFrontendOrigins",
            builder =>
            {
                builder.WithOrigins(FRONTEND_LOCAL_URL, FRONTEND_DOCKER_URL)
                       .AllowAnyHeader()
                       .AllowAnyMethod()
                       .AllowCredentials();
            });
    });
builder.Services.AddAuthorization();
builder.Services.AddIdentityApiEndpoints<IdentityUser>().AddEntityFrameworkStores<DataContext>();

var app = builder.Build();

// Configure the HTTP request pipeline.
// if (app.Environment.IsDevelopment())
// {
app.UseSwagger();
app.UseSwaggerUI();
// }
app.MapIdentityApi<IdentityUser>();

app.UseHttpsRedirection();

app.UseCors("AllowFrontendOrigins");

app.UseAuthorization();

app.MapControllers();

app.Run();
