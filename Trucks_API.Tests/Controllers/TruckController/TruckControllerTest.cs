using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Trucks_API.Controllers;
using Trucks_API.Data;
using Trucks_API.Dtos.Truck;
using Trucks_API.Models;
using Trucks_API.Services.TruckService;
using Xunit;


namespace Trucks_API.Tests.Controllers
{
    public class TruckControllerTests : IDisposable
    {
        private readonly DataContext _context;
        private readonly TruckService _truckService;
        private readonly IMapper _mapper;
        private readonly TruckController _controller;


        public void Dispose()
        {
            if (_context != null)
            {
                _context.Database.EnsureDeleted();
                _context.Dispose();
            }
        }

        public TruckControllerTests()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase")
                .Options;
            var _context = new DataContext(options);

            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Truck, GetTruckDto>();
                cfg.CreateMap<AddTruckDto, Truck>();
                cfg.CreateMap<UpdateTruckDto, Truck>();
            });
            _mapper = configuration.CreateMapper();

            _truckService = new TruckService(_mapper, _context);
            _controller = new TruckController(_truckService);
        }


        [Fact]
        public async Task Get_ReturnsOKStatusCode()
        {
            var result = await _controller.Get();

            Assert.IsType<OkObjectResult>(result.Result);
            Assert.Equal(200, ((OkObjectResult)result.Result).StatusCode);
        }

        [Fact]
        public async Task GetSingle_WithValidId_ReturnsOKStatusCode()
        {
            var truckToAdd = new AddTruckDto { Tare = 1000, LoadCapacity = 5000 };
            var addResult = await _controller.AddTruck(truckToAdd);
            var serviceResponse = (ServiceResponse<GetTruckDto>)((ObjectResult)addResult.Result).Value;

            Assert.True(serviceResponse.Success);
            Assert.NotNull(serviceResponse.Data);

            var truckId = serviceResponse.Data.Id;

            var result = await _controller.GetSingle(truckId);

            Assert.IsType<OkObjectResult>(result.Result);
            Assert.Equal(200, ((OkObjectResult)result.Result).StatusCode);

            Assert.NotNull(serviceResponse);
            Assert.NotNull(serviceResponse.Data);
            Assert.IsType<GetTruckDto>(serviceResponse.Data);

            var returnedTruck = serviceResponse.Data;
            Assert.Equal(truckToAdd.Tare, returnedTruck.Tare);
            Assert.Equal(truckToAdd.LoadCapacity, returnedTruck.LoadCapacity);

        }

        [Fact]
        public async Task GetSingle_WithInvalidId_ReturnsNotFoundStatusCode()
        {
            var result = await _controller.GetSingle(9999);

            Assert.IsType<NotFoundObjectResult>(result.Result);
            Assert.Equal(404, ((NotFoundObjectResult)result.Result).StatusCode);
        }

        [Fact]
        public async Task FilterTrucks_ReturnsOKStatusCode()
        {
            var truckToAdd1 = new AddTruckDto { Tare = 1000, LoadCapacity = 5000 };
            var addResult1 = await _controller.AddTruck(truckToAdd1);

            var truckToAdd2 = new AddTruckDto { Tare = 2000, LoadCapacity = 7000 };
            var addResult2 = await _controller.AddTruck(truckToAdd2);


            var filterDto = new TruckFilterDto
            {
                PageSize = 1,
                LoadCapacityUpperBound = 6000
            };

            var result = await _controller.FilterTrucks(filterDto);

            Assert.IsType<OkObjectResult>(result.Result);
            Assert.Equal(200, ((OkObjectResult)result.Result).StatusCode);
            var serviceResponse = (ServiceResponse<List<GetTruckDto>>)((OkObjectResult)result.Result).Value;
            Assert.NotNull(serviceResponse);
            Assert.NotNull(serviceResponse.Data);
            Assert.IsType<List<GetTruckDto>>(serviceResponse.Data);
            Assert.Single(serviceResponse.Data);

            foreach (var truck in serviceResponse.Data)
            {
                Assert.True(truck.LoadCapacity < 6000);
            }
        }

        [Fact]
        public async Task AddTruck_ReturnsOKStatusCode()
        {
            var truckToAdd = new AddTruckDto { Tare = 1000, LoadCapacity = 5000 };

            var result = await _controller.AddTruck(truckToAdd);

            Assert.IsType<OkObjectResult>(result.Result);
            Assert.Equal(200, ((OkObjectResult)result.Result).StatusCode);
        }

        [Fact]
        public async Task UpdateTruck_WithValidData_ReturnsOKStatusCode()
        {
            var truckToAdd = new AddTruckDto { Tare = 1000, LoadCapacity = 5000 };
            var addResult = await _controller.AddTruck(truckToAdd);
            var serviceResponse = (ServiceResponse<GetTruckDto>)((ObjectResult)addResult.Result).Value;

            Assert.True(serviceResponse.Success);
            Assert.NotNull(serviceResponse.Data);

            var truckId = serviceResponse.Data.Id;
            var updatedTruckDto = new UpdateTruckDto { Id = truckId, Tare = 1800, LoadCapacity = 5800 };

            var result = await _controller.UpdateTruck(updatedTruckDto);

            Assert.IsType<OkObjectResult>(result.Result);
            Assert.Equal(200, ((OkObjectResult)result.Result).StatusCode);

            var updatedTruckResult = await _controller.GetSingle(truckId);
            var updatedServiceResponse = (ServiceResponse<GetTruckDto>)((OkObjectResult)updatedTruckResult.Result).Value;

            Assert.Equal(updatedTruckDto.Tare, updatedServiceResponse.Data.Tare);
        }

        [Fact]
        public async Task UpdateTruck_WithInvalidData_ReturnsNotFoundStatusCode()
        {
            var updatedTruckDto = new UpdateTruckDto { Id = 9999, Tare = 1800, LoadCapacity = 5800 };

            var result = await _controller.UpdateTruck(updatedTruckDto);

            Assert.IsType<NotFoundObjectResult>(result.Result);
            Assert.Equal(404, ((NotFoundObjectResult)result.Result).StatusCode);
        }

        [Fact]
        public async Task DeleteTruck_WithValidId_ReturnsOKStatusCode()
        {
            var truckToAdd = new AddTruckDto { Tare = 1000, LoadCapacity = 5000 };
            var addResult = await _controller.AddTruck(truckToAdd);
            var serviceResponse = (ServiceResponse<GetTruckDto>)((ObjectResult)addResult.Result).Value;
            Assert.True(serviceResponse.Success);
            Assert.NotNull(serviceResponse.Data);
            var truckId = serviceResponse.Data.Id;

            var result = await _controller.DeleteTruck(truckId);

            Assert.IsType<OkObjectResult>(result.Result);
            Assert.Equal(200, ((OkObjectResult)result.Result).StatusCode);
        }

        [Fact]
        public async Task DeleteTruck_WithInvalidId_ReturnsNotFoundStatusCode()
        {
            var result = await _controller.DeleteTruck(9999);

            Assert.IsType<NotFoundObjectResult>(result.Result);
            Assert.Equal(404, ((NotFoundObjectResult)result.Result).StatusCode);
        }
    }
}
