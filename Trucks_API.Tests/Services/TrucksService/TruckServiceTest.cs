global using Trucks_API.Models;
global using Trucks_API.Services;
global using Trucks_API.Services.TruckService;
global using Trucks_API.Dtos.Truck;
global using AutoMapper;
global using Microsoft.EntityFrameworkCore;
global using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
global using Trucks_API.Data;
global using Microsoft.AspNetCore.Identity;
global using Microsoft.OpenApi.Models;
global using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Trucks_API.Models;
using Trucks_API.Services.TruckService;
using Xunit;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;



namespace Trucks_API.Tests.Services.TrucksService

{
    public class TruckServiceTests
    {
        private readonly TruckService _truckService;
        private readonly IMapper _mapper;

        public TruckServiceTests()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase")
                .Options;
            var dbContext = new DataContext(options);

            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Truck, GetTruckDto>();
                cfg.CreateMap<AddTruckDto, Truck>();
                cfg.CreateMap<UpdateTruckDto, Truck>();
            });
            _mapper = configuration.CreateMapper();

            _truckService = new TruckService(_mapper, dbContext);
        }

        [Fact]
        public async void GetAllTrucks_ReturnsListOfTrucks()
        {
            var result = await _truckService.GetAllTrucks();

            Assert.NotNull(result);
            Assert.NotNull(result.Data);
            Assert.IsType<List<GetTruckDto>>(result.Data);
        }

        [Fact]
        public async void AddTruck_ReturnsAddedTruck()
        {
            var newTruck = new AddTruckDto { Tare = 1000, LoadCapacity = 5000 };

            var result = await _truckService.AddTruck(newTruck);
            Assert.NotNull(result);
            Assert.NotNull(result.Data);
            Assert.IsType<GetTruckDto>(result.Data);
        }

        [Fact]
        public async Task UpdateTruck_ReturnsUpdatedTruck()
        {
            var newTruck = new AddTruckDto { Tare = 1000, LoadCapacity = 5000 };
            var serviceResponse = await _truckService.AddTruck(newTruck);

            Assert.True(serviceResponse.Success);
            Assert.NotNull(serviceResponse.Data);

            var truckId = serviceResponse.Data.Id;
            var updatedTruckDto = new UpdateTruckDto { Id = truckId, Tare = 1800, LoadCapacity = 5800 };
            var result = await _truckService.UpdateTruck(updatedTruckDto);

            Assert.NotNull(result);
            Assert.NotNull(result.Data);
            Assert.IsType<GetTruckDto>(result.Data);
            Assert.Equal(updatedTruckDto.Tare, result.Data.Tare);
            Assert.Equal(updatedTruckDto.LoadCapacity, result.Data.LoadCapacity);
        }

        [Fact]
        public async Task DeleteTruck_ReturnsDeletedTruck()
        {

            var newTruck = new AddTruckDto { Tare = 1000, LoadCapacity = 5000 };
            await _truckService.AddTruck(newTruck);

            var trucksBeforeDeletion = await _truckService.GetAllTrucks();
            var truckIdToDelete = trucksBeforeDeletion.Data.Last().Id;

            var result = await _truckService.DeleteTruck(truckIdToDelete);

            Assert.NotNull(result);
            Assert.NotNull(result.Data);
            Assert.IsType<GetTruckDto>(result.Data);
        }
    }
}