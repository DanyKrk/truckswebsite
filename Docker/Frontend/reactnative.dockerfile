FROM node:20.12.2-alpine3.19

WORKDIR /app

COPY ../../Trucks_App/package.json ./

RUN npm install --legacy-peer-deps

COPY ../../Trucks_App .

EXPOSE 3000

CMD [ "npx", "expo", "start", "--web"]