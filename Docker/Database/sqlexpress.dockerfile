FROM debian:buster AS build0
WORKDIR /root

RUN apt-get update && apt-get install -y binutils gcc

ADD wrapper.c /root/
RUN gcc -shared  -ldl -fPIC -o wrapper.so wrapper.c


FROM mcr.microsoft.com/mssql/server:2017-latest-ubuntu
COPY --from=build0 /root/wrapper.so /root/

USER root

ENV ACCEPT_EULA=Y
ENV SA_PASSWORD=Str0ngPasswordaaaaaa!

COPY init.sh /tmp/init.sh
COPY wait-for-it.sh /tmp/wait-for-it.sh
COPY initialization.sql /tmp/initialization.sql
COPY entrypoint.sh /tmp/entrypoint.sh

RUN chmod +x /tmp/init.sh /tmp/wait-for-it.sh /tmp/entrypoint.sh

CMD /bin/bash /tmp/entrypoint.sh