#!/bin/bash

SQL_SERVER_HOST=localhost
SQL_SERVER_PORT=1433

# Path to the wait-for-it script
WAIT_FOR_IT_SCRIPT=./tmp/wait-for-it.sh

# Wait for the SQL Server database to become available
${WAIT_FOR_IT_SCRIPT} ${SQL_SERVER_HOST}:${SQL_SERVER_PORT} 

/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P Str0ngPasswordaaaaaa! -d master -i /tmp/initialization.sql -I