#!/bin/bash

# Run init-script and make it run in the background
/tmp/init.sh &
# Start SQL server
LD_PRELOAD=/root/wrapper.so /opt/mssql/bin/sqlservr